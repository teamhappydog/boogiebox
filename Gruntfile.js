/******************************************************
 * Boogiebox Grunt
 * Let's get this started
 *
 ******************************************************/

module.exports = function (grunt) {

    // load all grunt tasks
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-postcss');

    grunt.initConfig({

        /******************************************************
         * Compile SASS
         ******************************************************/
        sass: {
            dist: {
                options: {
                    sourcemap: 'none',
                    style: 'compressed'
                },
                files: {
                    'dist/css/boogie.min.css': 'assets/scss/template.scss'
                }
            },
            dev: {
                options: {
                    sourcemap: 'none',
                    style: 'expanded'
                },
                files: {
                    'dist/css/boogie.css': 'assets/scss/template.scss'
                }
            }
        },

        /******************************************************
         * Post-process CSS
         ******************************************************/
        postcss: {
            options: {
                map: false,
                processors: [
                  require('autoprefixer')({
                      browsers: [
                          'last 2 versions',
                          'IE 11'
                      ]
                  }),
                ]
            },
            dist: {
              src: 'dist/css/*.css'
            }
        },

        /******************************************************
         * SERVER AND WATCH TASKS
         ******************************************************/
        watch: {
            sass: {
                files: [
                    'assets/scss/*.scss'
                ],
                tasks: ['compile:scss']
            }
        },

    });

    /******************************************************
     * COMPOUND TASKS
     ******************************************************/
    grunt.registerTask('compile:scss', ['sass', 'postcss']);
    grunt.registerTask('default', ['compile:scss']);

};

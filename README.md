# Boogiebox #
This repo is for managing a stand-alone version of Boogiebox that can be managed independently of sites and their own styles.

Documentation: [http://boogiedocs.happydogdev.com/](http://boogiedocs.happydogdev.com/)

### How to deploy? ###

We should set this deployment to transfer the CSS / JS files manually to the HDWP server. [todo]
All future sites, any maybe slowly changing existing sites, can use the continually updated Boogiebox. NPM is our typical method for pulling this package in.

### What about Material? SemanticUI? Bootstrap? ###

Since this is independent, it should run fine. To avoid conflicts, check the order in which files are loaded.